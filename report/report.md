---
# Title page
title: "TP Linux From Scratch"
author: Fridez Lucas
date: \today
subject: "Markdown"
keywords: [Markdown, Example]
subtitle: "Conception OS"
desc: |
    Au sein du cours de Conception OS enseigné par Claudio Cortinovis se déroule un travail pratique à choix. 
    Le thème choisi pour ce présent projet se nomme LFS, où **Linux From Scratch**.
    L'étudiant créé alors sa propre distribution Linux et la personnalise à son goût en s'aidant du célèbre tutoriel *Linux LFS*.
    Ce présent document retranscrit le déroulement du travail pratique effectué.

typedoc: "Rapport"
lang: "fr"
titlepage: true
titlepage-color: "FFFFFF"
titlepage-text-color: "000000"
titlepage-rule-color: "FF0000"
titlepage-rule-height: 2
titlepage-background: "./../theme/frontpage.pdf"
page-background: "./../theme/page.pdf"
page-background-opacity: 1

# Info Bottom titlepage
supervisor: Claudio Cortinovis
version: 1.0.0


links-as-notes: true
numbersections: true

# Table des matières
toc: true
toc-depth: 2
lof: true
lot: true
lol: true
tblPrefix: tab
toc-title: Table des matières
lol-title: Liste des extraits de code
lof-title: Liste des figures
codes-title: Liste des codes
code-abrev: Code
listingtitle: Extrait de code

bibliography: bib-refs.bib
csl: ../theme/iso690-author-date-fr.csl

header-includes:
- |
  \usepackage{lmodern}
  \usepackage{xspace}
  \usepackage{listings}
---
