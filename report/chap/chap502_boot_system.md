## Rendre le système bootable

L'installation du système est désormais terminée. Il faut rendre le LFS bootable.

### Création du fichier fstab

```{.sh caption="Configuration du fichier fstab"}
cat > /etc/fstab << "EOF"
# Begin /etc/fstab

# file system  mount-point  type     options             dump  fsck
#                                                              order

/dev/sdb2      /            ext4    defaults            1     1
/dev/sdb1      swap         swap     pri=1               0     0
proc           /proc        proc     nosuid,noexec,nodev 0     0
sysfs          /sys         sysfs    nosuid,noexec,nodev 0     0
devpts         /dev/pts     devpts   gid=5,mode=620      0     0
tmpfs          /run         tmpfs    defaults            0     0
devtmpfs       /dev         devtmpfs mode=0755,nosuid    0     0

# End /etc/fstab
EOF
```

### Installer le noyau Linux

```{.sh caption="Compilation et installation du noyau"}
tar -xvf linux-5.10.17.tar.xz
cd linux-5.10.17

make mrproper
make menuconfig

# Configure kernel Linux in menuconfig

make
make modules_install
```

### Installer grub

Désormais, il est nécessaire d'installer grub pour booter sur le disque au démarrage de la machine.

```{.sh caption="Installation de grub"}
tar -xvf grub-2.04.tar.xz
cd grub

# Install grub on disk
grub-install /dev/sdb --target i386-pc

cat > /boot/grub/grub.cfg << "EOF"
# Begin /boot/grub/grub.cfg
set default=0
set timeout=5

insmod ext2
set root=(hd1,2)

menuentry "GNU/Linux, Linux 5.10.17-lfs-10.1" {
        linux   /boot/vmlinuz-5.10.17 root=/dev/sdb2 ro
}
EOF
```

### Informations du système LFS

Le système est bootable. Désormais, il convient de spécifier diverses informations sur le système, comme son nom et son code de version :

```{.sh caption="Informations sur le LFS installé"}
# LFS version number
echo 10.1 > /etc/lfs-release

# Distribution
cat > /etc/lsb-release << "EOF"
DISTRIB_ID="Linux From Scratch FRIDEZ"
DISTRIB_RELEASE="10.1"
DISTRIB_CODENAME="FRIDEZ OS"
DISTRIB_DESCRIPTION="Linux From Scratch Fridez"
EOF
``` 

Enfin, il convient de redémarrer la machine sur le disque secondaire et booter sur le système installé !

\newpage
