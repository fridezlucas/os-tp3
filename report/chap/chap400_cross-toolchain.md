# Build LFS Cross Toolchains et outils temporaires

Ce chapitre permet de préparer un système temporaire dans le but de compiler un LFS.
Pour se faire, divers outils sont installés dans le répertoire `$LFS/tools`.
Cette méthodologie permet de séparer le système LFS des outils de constructions nécessaire pour ce travail pratique. [@lfscross]

## Compilation Cross-Toolchain

Il convient de compiler des outils de cross-compilation et les outils associés.
Dans ce but, il est nécessaire de se rendre dans le dossier `/mnt/lfs/sources` via la commande
`cd $LFS/sources`.

### Binutils

Il convient de compiler en premier `binutils` car Glibc et gcc l'utilisent.

```{.sh caption="Compilation de binutils"}
tar -xvf binutils-2.36.1.tar.xz
cd binutils-2.36.1
mkdir -v build
cd build
../configure --prefix=$LFS/tools       \
             --with-sysroot=$LFS        \
             --target=$LFS_TGT          \
             --disable-nls              \
             --disable-werror

time make # to see building time
```

La sortie de la compilation est la suivante :

```{.sh caption="Sortie de compilation de make pour binutils"}
make[4]: Leaving directory '/mnt/lfs/sources/binutils-2.36.1/build/ld'
make[3]: Leaving directory '/mnt/lfs/sources/binutils-2.36.1/build/ld'
make[2]: Leaving directory '/mnt/lfs/sources/binutils-2.36.1/build/ld'
make[1]: Leaving directory '/mnt/lfs/sources/binutils-2.36.1/build'

real	0m44.204s
user	2m25.002s
sys	0m24.826s
```

Dès lors, il convient d'installer le package avec la commande `make install`.

Une fois binutils installé, les commandes suivantes sont nécessaires :

```{.sh caption="Finition de l'installation de binutils"}
ls -l $LFS/tools # To show installed tools

# Output
# drwxr-xr-x 2 lfs lfs 4096 Apr 11 14:41 bin
# drwxr-xr-x 3 lfs lfs 4096 Apr 11 14:41 lib
# drwxr-xr-x 4 lfs lfs 4096 Apr 11 14:41 share
# drwxr-xr-x 4 lfs lfs 4096 Apr 11 14:41 x86_64-lfs-linux-gnu

cd ../..
rm -Rf binutils
rm -Rf binutils-2.36.1 # Delete all used sources
```

### Installation de gcc

Afin d'installer gcc, les commandes suivantes sont nécessaires :

```{.sh caption="Installation de gcc"}
tar -xvf gcc-10.2.0.tar.xz # Extract source from tar file
cd gcc-12.2.0

tar -xf ../mpfr-4.1.0.tar.xz
mv -v mpfr-4.1.0 mpfr
tar -xf ../gmp-6.2.1.tar.xz
mv -v gmp-6.2.1 gmp
tar -xf ../mpc-1.2.1.tar.gz
mv -v mpc-1.2.1 mpc

# Set default directory to lib (x86_64 hosts)
case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' \
        -i.orig gcc/config/i386/t-linux64
 ;;
esac

mkdir -v build
cd build

# Prepare gcc for compilation
../configure                                       \
    --target=$LFS_TGT                              \
    --prefix=$LFS/tools                            \
    --with-glibc-version=2.11                      \
    --with-sysroot=$LFS                            \
    --with-newlib                                  \
    --without-headers                              \
    --enable-initfini-array                        \
    --disable-nls                                  \
    --disable-shared                               \
    --disable-multilib                             \
    --disable-decimal-float                        \
    --disable-threads                              \
    --disable-libatomic                            \
    --disable-libgomp                              \
    --disable-libquadmath                          \
    --disable-libssp                               \
    --disable-libvtv                               \
    --disable-libstdcxx                            \
    --enable-languages=c,c++

# Now compile gcc
time make
```

La sortie de la compilation est la suivante :

```{.sh caption="Sortie de la compilation de gcc"}
make[3]: Leaving directory '/mnt/lfs/sources/gcc-10.2.0/build/x86_64-lfs-linux-gnu/libgcc'
make[2]: Leaving directory '/mnt/lfs/sources/gcc-10.2.0/build/x86_64-lfs-linux-gnu/libgcc'
make[1]: Leaving directory '/mnt/lfs/sources/gcc-10.2.0/build'

real	6m21.709s
user	29m14.577s
sys	2m20.490s
```

Il convient désormais d'installer gcc via la commande `make install`.

Enfin, les commandes suivantes terminent son installation :

```{.sh caption="Fin de l'installation de gcc"}
cd ..

# Create a necessary header file
cat gcc/limitx.h gcc/glimits.h gcc/limity.h > \
  `dirname $($LFS_TGT-gcc -print-libgcc-file-name)`/install-tools/include/limits.h
cd ..
rm -Rf gcc-10.2.0
```

### Installation des header d'API Linux

Afin d'installer les headers requis, les commandes suivantes sont à exécuter :

```{.sh caption="Installation des headers d'API Linux"}
tar -xvf linux-5.10.17.tar.xz
cd linux-5.10.17

# Make sur that no stale files are present in package
make mrproper

make headers
find usr/include -name '.*' -delete
rm usr/include/Makefile
cp -rv usr/include $LFS/usr

cd ..
rm -Rf linux-5.10.17
```

### Installation de Glibc

Afin d'installer glibc, les commandes suivantes sont à exécuter :

```{.sh caption="Installation de Glibc"}
tar -xvf glibc-2.33.tar.xz
cd glibc-2.33

# Create a symbolic link for LSB compliance.
# for x86_64, create a compatibility symbolic link required
case $(uname -m) in
    i?86)   ln -sfv ld-linux.so.2 $LFS/lib/ld-lsb.so.3
    ;;
    x86_64) ln -sfv ../lib/ld-linux-x86-64.so.2 $LFS/lib64
            ln -sfv ../lib/ld-linux-x86-64.so.2 $LFS/lib64/ld-lsb-x86-64.so.3
    ;;
esac

# Apply downloaded patch
patch -Np1 -i ../glibc-2.33-fhs-1.patch

# Prepare build
mkdir -v build
cd build

# Configure glibc compilation
../configure                             \
      --prefix=/usr                      \
      --host=$LFS_TGT                    \
      --build=$(../scripts/config.guess) \
      --enable-kernel=3.2                \
      --with-headers=$LFS/usr/include    \
      libc_cv_slibdir=/lib

# Compile
time make
```

La sortie de la compilation est la suivante :

```{.sh caption="Sortie de la compilation de glibc"}
make[2]: Leaving directory '/mnt/lfs/sources/glibc-2.33/elf'
make[1]: Leaving directory '/mnt/lfs/sources/glibc-2.33'

real	2m28.686s
user	9m17.070s
sys	2m2.297s
```

Désormais, il convient d'installer le package avec la commande `make DESTDIR=$LFS install`.

### Validation d'installation

Afin de valider les précédentes installations, il convient de lancer les commandes suivantes (qui testent un programme `C`) :

```{.sh caption="Validation d'installation de packages"}
echo 'int main(){}' > dummy.c
$LFS_TGT-gcc dummy.c
readelf -l a.out | grep '/ld-linux'
```

La sortie est la suivante : `[Requesting program interpreter: /lib64/ld-linux-x86-64.so.2]`. Elle correspond à la valeur recherchée pour valider une installation sur un système `x86_64`.

Il est désormais possible de terminer l'installation des headers avec la commande suivante :

```{.sh caption="Finaliser l'installer du header limit.h"}
$LFS/tools/libexec/gcc/$LFS_TGT/10.2.0/install-tools/mkheaders

# Delete unnecessary files
cd ../..
rm -Rf glibc-2.33
```

### Installation de libstdc++

Afin d'installer libstdc++, il convient de lancer les commandes suivantes :

```{.sh caption="Installation de libstdc++"}
tar -xvf gcc-10.2.0.tar.xz
cd gcc-10.2.0

mkdir -v build
cd build

# Prepare libstdc++ compilation
../libstdc++-v3/configure           \
    --host=$LFS_TGT                 \
    --build=$(../config.guess)      \
    --prefix=/usr                   \
    --disable-multilib              \
    --disable-nls                   \
    --disable-libstdcxx-pch         \
    --with-gxx-include-dir=/tools/$LFS_TGT/include/c++/10.2.0

# Compile and install
make
make DESTDIR=$LFS install

# Remove unnecessary files
cd ../..
rm -Rf gcc-10.2.0
```

\newpage
