# Préparation du build

Ce présent chapitre vise à préparer la machine dans le but de compiler un Linux.

## Préparation de la machine hôte

Durant cette phase, il convient de vérifier que la machine hôte détient tous les utilitaires nécessaires pour la compilation.

### Contrôle des pré-requis

Ainsi, un script `check-requirements.sh` est créé. Son contenu est le suivant :

```{.sh caption="Contenu du script check-requirements.sh"}
#!/bin/bash
# Simple script to list version numbers of critical development tools
export LC_ALL=C
bash --version | head -n1 | cut -d" " -f2-4
MYSH=$(readlink -f /bin/sh)
echo "/bin/sh -> $MYSH"
echo $MYSH | grep -q bash || echo "ERROR: /bin/sh does not point to bash"
unset MYSH

echo -n "Binutils: "; ld --version | head -n1 | cut -d" " -f3-
bison --version | head -n1

if [ -h /usr/bin/yacc ]; then
  echo "/usr/bin/yacc -> `readlink -f /usr/bin/yacc`";
elif [ -x /usr/bin/yacc ]; then
  echo yacc is `/usr/bin/yacc --version | head -n1`
else
  echo "yacc not found" 
fi

bzip2 --version 2>&1 < /dev/null | head -n1 | cut -d" " -f1,6-
echo -n "Coreutils: "; chown --version | head -n1 | cut -d")" -f2
diff --version | head -n1
find --version | head -n1
gawk --version | head -n1

if [ -h /usr/bin/awk ]; then
  echo "/usr/bin/awk -> `readlink -f /usr/bin/awk`";
elif [ -x /usr/bin/awk ]; then
  echo awk is `/usr/bin/awk --version | head -n1`
else 
  echo "awk not found" 
fi

gcc --version | head -n1
g++ --version | head -n1
ldd --version | head -n1 | cut -d" " -f2-  # glibc version
grep --version | head -n1
gzip --version | head -n1
cat /proc/version
m4 --version | head -n1
make --version | head -n1
patch --version | head -n1
echo Perl `perl -V:version`
python3 --version
sed --version | head -n1
tar --version | head -n1
makeinfo --version | head -n1  # texinfo version
xz --version | head -n1

echo 'int main(){}' > dummy.c && g++ -o dummy dummy.c
if [ -x dummy ]
  then echo "g++ compilation OK";
  else echo "g++ compilation failed"; fi
rm -f dummy.c dummy
```

Dès lors, il convient de le rendre exécutable et de le tester. Les commandes suivantes sont nécessaires :

```{.sh caption="Rendre check-requirements.sh exécutable"}
chmod +x check-requirements.sh
./check-requirements.sh
```

La sortie du script est la suivante :

```{.sh caption="Résultat du script check-requirements sur la machine virtuelle"}
bash, version 5.1.4(1)-release
/bin/sh -> /usr/bin/bash
Binutils: (GNU Binutils) 2.35.1
bison (GNU Bison) 3.7.2
yacc is bison (GNU Bison) 3.7.2
bzip2,  Version 1.0.8, 13-Jul-2019.
Coreutils:  8.32
diff (GNU diffutils) 3.7
find (GNU findutils) 4.7.0
GNU Awk 5.1.0, API: 3.0 (GNU MPFR 4.1.0, GNU MP 6.2.1)
/usr/bin/awk -> /usr/bin/gawk
gcc (GCC) 10.2.0
g++ (GCC) 10.2.0
(GNU libc) 2.32
grep (GNU grep) 3.6
gzip 1.10
Linux version 5.10.11-arch1-1 (linux@archlinux) (gcc (GCC) 10.2.0, GNU ld (GNU Binutils) 2.35.1) #1 SMP PREEMPT Wed, 27 Jan 2021 13:53:16 +0000
m4 (GNU M4) 1.4.18
GNU Make 4.3
GNU patch 2.7.6
Perl version='5.32.0';
Python 3.9.1
sed (GNU sed) 4.8
tar (GNU tar) 1.33
texi2any (GNU texinfo) 6.7
xz (XZ Utils) 5.2.5
g++ compilation OK
```


\newpage

### Création d'une partition dédiée

Afin de monter un Linux, il convient de créer une partition dédiée. Pour se faire, il est nécessaire d'être en *super user* :

```{.sh caption="Entrer en mode super user"}
sudo su -
# Enter its own password to enter in super user mode
```

Puis, il faut obtenir une liste des disques disponibles pour créer une partition :

```{.sh caption="Lister les disques disponibles"}
fdisk -l
```

Le résultat de la commande `fdisk` est la suivante :

```{.sh caption="Liste des disques et partitions"}
Disk /dev/sda: 30 GiB, 32212254720 bytes, 62914560 sectors
Disk model: VBOX HARDDISK   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x80fa0da6

Device     Boot    Start      End  Sectors Size Id Type
/dev/sda1  *        2048 56619689 56617642  27G 83 Linux
/dev/sda2       56619690 62910539  6290850   3G 82 Linux swap / Solaris


Disk /dev/sdb: 30 GiB, 32212254720 bytes, 62914560 sectors
Disk model: VBOX HARDDISK   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
```

Un disque secondaire est disponible `/dev/sdb`.
Ce dernier est utilisé pour créer une partition dédiée :

```{.sh caption="Créer une partition swap"}
fdisk /dev/sdb

# Now in fdisk utility

# type n to create a new partition
# type p to create a primary partition
# type 1 to define 1 as partition number
# type enter to use first available sector
# type +1G to define a partition of 1Gb
# type t to define partition type
# type 82 to define partition as SWAP
# type p to see defined partition
```

La sortie des commandes ci-dessus est la suivante :

```{.sh caption="Résultat de création d'une partition swap"}
[root@lfs-fridez ~]# fdisk /dev/sdb

Welcome to fdisk (util-linux 2.36.1).
Changes will remain in memory only, until you decide to write them.
Be careful before using the write command.

Device does not contain a recognized partition table.
Created a new DOS disklabel with disk identifier 0x9ef9c460.

Command (m for help): n
Partition type
   p   primary (0 primary, 0 extended, 4 free)
   e   extended (container for logical partitions)
Select (default p): p
Partition number (1-4, default 1): 1
First sector (2048-62914559, default 2048): 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2048-62914559, default 62914559): +1G

Created a new partition 1 of type 'Linux' and of size 1 GiB.

Command (m for help): t
Selected partition 1
Hex code or alias (type L to list all): 82
Changed type of partition 'Linux' to 'Linux swap / Solaris'.

Command (m for help): p
Disk /dev/sdb: 30 GiB, 32212254720 bytes, 62914560 sectors
Disk model: VBOX HARDDISK   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x9ef9c460

Device     Boot Start     End Sectors Size Id Type
/dev/sdb1        2048 2099199 2097152   1G 82 Linux swap / Solaris
``` 

Désormais, une partition principale est nécessaire.
Cette dernière est créée via `fdisk` également :

```{.sh caption="Création d'une partition primare"}
fdisk /dev/sdb

# Enter in fdisk mode

# type n to create a new partition
# type p to create a primary partition
# type 2 to define 2 as partition number
# type enter to use first available sector
# type to define a partition with all defined available space
# type t to define partition type
# type 82 to define partition as SWAP
# type p to see defined partition
```

Le sortie de la création est la suivante : 

```{.sh caption="Sortie de la création de la partition principale"}
Command (m for help): n
Partition type
   p   primary (1 primary, 0 extended, 3 free)
   e   extended (container for logical partitions)
Select (default p): p
Partition number (2-4, default 2): 2
First sector (2099200-62914559, default 2099200): 
Last sector, +/-sectors or +/-size{K,M,G,T,P} (2099200-62914559, default 62914559): 

Created a new partition 2 of type 'Linux' and of size 29 GiB.

Command (m for help): p
Disk /dev/sdb: 30 GiB, 32212254720 bytes, 62914560 sectors
Disk model: VBOX HARDDISK   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x9ef9c460

Device     Boot   Start      End  Sectors Size Id Type
/dev/sdb1          2048  2099199  2097152   1G 82 Linux swap / Solaris
/dev/sdb2       2099200 62914559 60815360  29G 83 Linux
```

Toujours dans `fdisk`, entrer la commande `w` pour écrire les partitions définies. 

Enfin, vérifier le tout avec une nouvelle exécution de la commande `fdisk -l` :

```{.sh caption="Partitions nouvellement créées"}
[root@lfs-fridez ~]# fdisk -l
Disk /dev/sda: 30 GiB, 32212254720 bytes, 62914560 sectors
Disk model: VBOX HARDDISK   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x80fa0da6

Device     Boot    Start      End  Sectors Size Id Type
/dev/sda1  *        2048 56619689 56617642  27G 83 Linux
/dev/sda2       56619690 62910539  6290850   3G 82 Linux swap / Solaris


Disk /dev/sdb: 30 GiB, 32212254720 bytes, 62914560 sectors
Disk model: VBOX HARDDISK   
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
Disklabel type: dos
Disk identifier: 0x9ef9c460

Device     Boot   Start      End  Sectors Size Id Type
/dev/sdb1          2048  2099199  2097152   1G 82 Linux swap / Solaris
/dev/sdb2       2099200 62914559 60815360  29G 83 Linux

```

### Formattage des partitions

Une fois les partitions créées, il convient de les formatter via les commandes suivantes :

```{.sh caption="Montage des partitions créées"}
mkfs -v -t ext4 /dev/sdb1
mkswap /dev/sdb1

mkfs -v -t ext4 /dev/sdb2
```

### Attribuer une variable pour le chemin d'accès

Dans le but de rendre plus simple les chemins d'accès aux montages des disques,
une variable `LFS` est défini dans le fichier `~/.bashrc` : 

```{.sh caption="Définition d'une variable LFS"}
LFS=/mnt/lfs.
```

### Monter les partitions

Désormais, il convient de monter les partitions sur le système :

```{.sh caption="Monter les partitions sur le système"}
# Primary partition

mkdir -pv $LFS
mount -v -t ext4 /dev/sdb2 $LFS

# Swap partition
/sbin/swapon -v /dev/sdb1
```

Puis, intégrer dans le fichier `/etc/fstab` le montage de la partition `/dev/sdb2` :

```{.sh caption=""}
/dev/sdb2       /mnt/lfs        ext4    defaults        1       1
```