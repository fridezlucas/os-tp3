## Configuration du système

Cette section configure le système avec divers paramètres :

- Nom de machine
- Paramètres réseaux
- Paramètres clavier, langue, horloge

### Scripts de démarrage

Il convient d'installer les scripts de démarrage pour le LFS : 

```{.sh caption="Installation des scripts de démarrage"}
# in /sources in chroot
tar -xvf lfs-bootscript-20210201.tar.xz
cd lfs-bootscript-20210201
make install
cd ..
rm -rf lfs-bootscript-20210201
```

### Configuration réseau

Puis, de créer une configuration réseau :

```{.sh caption="Création de la configuration réseau"}
cd /etc/sysconfig/
cat > ifconfig.eth0 << "EOF"
ONBOOT=yes
IFACE=eth0
SERVICE=ipv4-static
IP=192.168.1.240
GATEWAY=192.168.1.1
PREFIX=24
BROADCAST=192.168.1.255
EOF

cat > /etc/resolv.conf << "EOF"
# Begin /etc/resolv.conf

domain fridez.dev
nameserver 192.168.1.1
nameserver 8.8.8.8

# End /etc/resolv.conf
EOF

echo "lfs-lfridez" > /etc/hostname

cat > /etc/hosts << "EOF"
# Begin /etc/hosts

127.0.0.1 localhost.localdomain localhost
127.0.1.1 lfs.fridez.dev lfs-lfridez
::1       localhost ip6-localhost ip6-loopback
ff02::1   ip6-allnodes
ff02::2   ip6-allrouters

# End /etc/hosts
EOF
```

\newpage

### Sysvinit

Ensuite, il convient de configurer Sysvinit via le script ci-dessous : 

```{.sh caption="Configurer Sysvinit"}
cat > /etc/inittab << "EOF"
# Begin /etc/inittab

id:3:initdefault:

si::sysinit:/etc/rc.d/init.d/rc S

l0:0:wait:/etc/rc.d/init.d/rc 0
l1:S1:wait:/etc/rc.d/init.d/rc 1
l2:2:wait:/etc/rc.d/init.d/rc 2
l3:3:wait:/etc/rc.d/init.d/rc 3
l4:4:wait:/etc/rc.d/init.d/rc 4
l5:5:wait:/etc/rc.d/init.d/rc 5
l6:6:wait:/etc/rc.d/init.d/rc 6

ca:12345:ctrlaltdel:/sbin/shutdown -t1 -a -r now

su:S016:once:/sbin/sulogin

1:2345:respawn:/sbin/agetty --noclear tty1 9600
2:2345:respawn:/sbin/agetty tty2 9600
3:2345:respawn:/sbin/agetty tty3 9600
4:2345:respawn:/sbin/agetty tty4 9600
5:2345:respawn:/sbin/agetty tty5 9600
6:2345:respawn:/sbin/agetty tty6 9600

# End /etc/inittab
EOF
```

\newpage

### Configuration locales

La configuration est effectuée comme le démontre le tableau ci-dessous : 

: Configuration des paramètres locaux

| Type     | Valeur        |
|----------|---------------|
| Locale   | en_US         |
| Timezone | Europe/Zurich |
| Clavier  | fr_CH         |

### Configuration shell

Les shells sont configurés de la manière suivante :

```{.sh caption="Configuration des shells"}
cat > /etc/shells << "EOF"
# Begin /etc/shells

/bin/sh
/bin/bash

# End /etc/shells
EOF
```

\newpage
