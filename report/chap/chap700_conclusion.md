# Fin du tutoriel

Le tutoriel est terminé. Le système LFS est fonctionnel, comme le démontrent les captures d'écran ci-dessous :

![Version de l'OS installée](img/lfs-end.png)

![Exécution d'un script bash dans le LFS installé](img/lfs-bash.png)

![Compilation et exécution d'un fichier cpp dans le LFS installé](img/lfs-cpp.png)

![Système LFs lié au réseau local (via adaptateur Bridge)](img/lfs-ping.png)

\newpage

# Conclusion

La **première partie** du TP, soit la mise à disposition d'une machine virtuelle,
s'est déroulée correctement. Le premier TP du cours de Conception OS permet une mise en route rapide.
En somme, une VM avec interface graphique et deux disques durs est créée.

Lors de la **deuxième partie**, de nombreuses commandes sont à entrer pour créer un système temporaire de compilation.
Cette partie permet de bien comprendre comment est construit un système Linux. De nombreux paquets sont nécessaires pour compiler un système.

La **troisième partie** retranscrit l'installation et la configuration d'un tel système.
Ici, de nombreux paquets sont installés pour obtenir un système complet : d'un exécuteur bash jusqu'à l'interpréteur Python en passant par le compilateur gcc.
En somme, un système Linux demande énormément de paquets pour fonctionner.
En effet, sur un tel système, **tout est fichier**.
Il en résulte un nombre d'installations de paquets importants.

la **quatrième partie** permet de configurer le système fraîchement construit.
La configuration réseau, les informations de la distribution créée ainsi que divers paramètres
d'utilisations, tels que langue, clavier et heures sont définis.
De plus, une compilation du noyau Linux est nécessaire.
Cette manipulation s'effectue aisément car déjà pratiquée à travers le TP02.
Pour finir, il convient de rendre le système *bootable* en installant et configurant *GRUB*.

Enfin, il est temps de tester le système construit. Celui-ci est testé via l'utilisation de divers paquets installés :

- ping
- gcc
- bash
- vim
- python3

En somme, le TP s'est déroulé sans rencontrer de problèmes.
La construction d'un système LFS via des tutoriels n'est pas difficile.
Cela nécessite tout de même une concentration élevée et une bonne connaissance de l'utilisation d'un système Linux en général.


\newpage

# Journal de travail

Ce chapitre liste les journées de travail ainsi que leur thème :

: Journées effectuées pour le TP 03

| Jour          | Thème                      |
|---------------|----------------------------|
| 6 avril 2021  | Création machine virtuelle |
| 7 avril 2021  | Chapitres 1 & 2            |
| 8 avril 2021  | Chapitre 3                 |
| 9 avril 2021  | Chapitre 4                 |
| 10 avril 2021 | Chapitre 5                 |
| 12 avril 2021 | Chapitres 6 et 7           |
| 13 avril 2021 | Chapitre 8                 |
| 14 avril 2021 | Chapitre 8 fin             |
| 15 avril 2021 | Chapitre 9, 10, 11         |

\newpage

# Références 


<!--<a href="https://fr.pikbest.com">Libre  modèles de fr.pikbest.com</a> -->