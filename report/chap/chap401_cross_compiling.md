## Compilations d'outils additionnels 

Cette section utilise les programmes de compilation installés au sein de la précédente section pour construire les utilitaires de base.
Ces derniers seront à leur emplacement final et pourront dès la prochain section être utilisés. [@temptools]

### Installation de M4

```{.sh caption="Installation de M4"}
tar -xvf m4-1.4.18.tar.xz
cd m4-1.4.18

# Do some fixes
sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' lib/*.c
echo "#define _IO_IN_BACKUP 0x100" >> lib/stdio-impl.h

# Prepare M4 compilation
./configure --prefix=/usr   \
            --host=$LFS_TGT \
            --build=$(build-aux/config.guess)

# Compile and install
make
make DESTDIR=$LFS install

# Remove unnecessary files
cd ..
rm -Rf m4-1.4.18
```

\newpage

### Installation de Ncurses

```{.sh caption="Installation de NCurse"}
tar -xvf tar -xvf ncurses-6.2.tar.gz
cd tar -xvf ncurses-6.2

# Ensure that gawk is found
sed -i s/mawk// configure

# Build tic program on the build host
mkdir build
pushd build
  ../configure
  make -C include
  make -C progs tic
popd

# Prepare NCurse for compilation
./configure --prefix=/usr                \
            --host=$LFS_TGT              \
            --build=$(./config.guess)    \
            --mandir=/usr/share/man      \
            --with-manpage-format=normal \
            --with-shared                \
            --without-debug              \
            --without-ada                \
            --without-normal             \
            --enable-widec

# Compile and install
make
make DESTDIR=$LFS TIC_PATH=$(pwd)/build/progs/tic install
echo "INPUT(-lncursesw)" > $LFS/usr/lib/libncurses.so

# Move created libraries and recreate symlinks
mv -v $LFS/usr/lib/libncursesw.so.6* $LFS/lib
ln -sfv ../../lib/$(readlink $LFS/usr/lib/libncursesw.so) $LFS/usr/lib/libncursesw.so

# Remove unnecessary files
cd ..
rm -Rf ncurses-6.2
```

\newpage

### Installation de Bash

```{.sh caption="Installation de Bash"}
tar -xvf bash-5.1.tar.gz
cd bash-5.1

# Prepare Bash compilation
./configure --prefix=/usr                   \
            --build=$(support/config.guess) \
            --host=$LFS_TGT                 \
            --without-bash-malloc

# Compile and install
make
make DESTDIR=$LFS install

# Move executable file and make link
mv $LFS/usr/bin/bash $LFS/bin/bash
ln -sv bash $LFS/bin/sh

# Remove unnecessary files
cd ..
rm -Rf bash-5.1
```

\newpage

### Installation de Coreutils

```{.sh caption="Installation de Coreutils"}
tar -xvf coreutils-8.32.tar.xz
cd coreutils-8.32

# Prepare Coreutils compilation
./configure --prefix=/usr                     \
            --host=$LFS_TGT                   \
            --build=$(build-aux/config.guess) \
            --enable-install-program=hostname \
            --enable-no-install-program=kill,uptime

# Compile and install
make
make DESTDIR=$LFS install

# Move program to final destination
mv -v $LFS/usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo} $LFS/bin
mv -v $LFS/usr/bin/{false,ln,ls,mkdir,mknod,mv,pwd,rm}        $LFS/bin
mv -v $LFS/usr/bin/{rmdir,stty,sync,true,uname}               $LFS/bin
mv -v $LFS/usr/bin/{head,nice,sleep,touch}                    $LFS/bin
mv -v $LFS/usr/bin/chroot                                     $LFS/usr/sbin
mkdir -pv $LFS/usr/share/man/man8
mv -v $LFS/usr/share/man/man1/chroot.1                        $LFS/usr/share/man/man8/chroot.8
sed -i 's/"1"/"8"/'                                           $LFS/usr/share/man/man8/chroot.8

# Remove unnecessary files
cd ..
rm -Rf coreutils-8.32
```

\newpage

### Installation de Diffutils

```{.sh caption="Installation de Diffutils"}
tar -xvf diffutils-3.7.tar.xz
cd diffutils-3.7

# Compile and install
make
make DESTDIR=$LFS install

# Remove unnecessary files
cd ..
rm -Rf diffutils-3.7
```

\newpage

### Installation de File

```{.sh caption="Installation de File"}
tar -xvf file-5.39.tar.gz
cd file-5.39

# Build correct version of File
mkdir build
pushd build
  ../configure --disable-bzlib      \
               --disable-libseccomp \
               --disable-xzlib      \
               --disable-zlib
  make
popd

# Prepare File compilation
./configure --prefix=/usr --host=$LFS_TGT --build=$(./config.guess)

# Compile and install
make FILE_COMPILE=$(pwd)/build/src/file
make DESTDIR=$LFS install

# Remove unnecessary files
cd ..
rm -Rf file-5.39
```

\newpage

### Installation de Findutils

```{.sh caption="Installation de Findutils"}
tar -xvf findutils-4.8.0.tar.gz
cd findutils-4.8.0

# Prepare Findutils compilation
./configure --prefix=/usr   \
            --host=$LFS_TGT \
            --build=$(build-aux/config.guess)

# Compile and install
make
make DESTDIR=$LFS install

# Move files to final destination
mv -v $LFS/usr/bin/find $LFS/bin
sed -i 's|find:=${BINDIR}|find:=/bin|' $LFS/usr/bin/updatedb

# Remove unnecessary files
cd ..
rm -Rf findutils-4.8.0
```

\newpage

### Installation de Gawk

```{.sh caption="Installation de Gawk"}
tar -xvf gawk-5.1.0.tar.xz
cd gawk-5.1.0

# Ensure some unneeded files are not installed
sed -i 's/extras//' Makefile.in

# Prepare Gawk compilation
./configure --prefix=/usr   \
            --host=$LFS_TGT \
            --build=$(./config.guess)

# Compile and install
make
make DESTDIR=$LFS install

# Remove unnecessary files
cd ..
rm -Rf gawk-5.1.0
```

\newpage

### Installation de Grep

```{.sh caption="Installation de Grep"}
tar -xvf grep-3.6.tar.xz
cd grep-3.6

# Ensure some unneeded files are not installed
sed -i 's/extras//' Makefile.in

# Prepare Grep compilation
./configure --prefix=/usr   \
            --host=$LFS_TGT \
            --bindir=/bin

# Compile and install
make
make DESTDIR=$LFS install

# Remove unnecessary files
cd ..
rm -Rf grep-3.6
```

\newpage

### Installation de Gzip

```{.sh caption="Installation de Gzip"}
tar -xvf gzip-1.10.tar.xz
cd gzip-1.10

# Prepare Gzip compilation
./configure --prefix=/usr --host=$LFS_TGT

# Compile and install
make
make DESTDIR=$LFS install

# Move file to final destination
mv -v $LFS/usr/bin/gzip $LFS/bin

# Remove unnecessary files
cd ..
rm -Rf gzip-1.10
```

\newpage

### Installation de Make

```{.sh caption="Installation de Make"}
tar -xvf make-4.3.tar.xz
cd make-4.3

# Prepare Make compilation
./configure --prefix=/usr   \
            --without-guile \
            --host=$LFS_TGT \
            --build=$(build-aux/config.guess)

# Compile and install
make
make DESTDIR=$LFS install

# Remove unnecessary files
cd ..
rm -Rf make-4.3
```

\newpage

### Installation de Patch

```{.sh caption="Installation de Patch"}
tar -xvf patch-2.7.6.tar.xz
cd patch-2.7.6

# Prepare Patch compilation
./configure --prefix=/usr   \
            --host=$LFS_TGT \
            --build=$(build-aux/config.guess)

# Compile and install
make
make DESTDIR=$LFS install

# Remove unnecessary files
cd ..
rm -Rf patch-2.7.6
```

\newpage

### Installation de Sed

```{.sh caption="Installation de Sed"}
tar -xvf sed-4.8.tar.xz
cd sed-4.8

# Prepare Sed compilation
./configure --prefix=/usr   \
            --host=$LFS_TGT \
            --bindir=/bin

# Compile and install
make
make DESTDIR=$LFS install

# Remove unnecessary files
cd ..
rm -Rf sed-4.8
```

\newpage

### Installation de Tar

```{.sh caption="Installation de Tar"}
tar -xvf tar-1.34.tar.xz
cd tar-1.34

# Prepare Tar compilation
./configure --prefix=/usr                     \
            --host=$LFS_TGT                   \
            --build=$(build-aux/config.guess) \
            --bindir=/bin

# Compile and install
make
make DESTDIR=$LFS install

# Remove unnecessary files
cd ..
rm -Rf tar-1.34
```

\newpage

### Installation de Xz

```{.sh caption="Installation de Xz"}
tar -xvf xz-5.2.5.tar.xz
cd xz-5.2.5

# Prepare Xz compilation
./configure --prefix=/usr                     \
            --host=$LFS_TGT                   \
            --build=$(build-aux/config.guess) \
            --disable-static                  \
            --docdir=/usr/share/doc/xz-5.2.5

# Compile and install
make
make DESTDIR=$LFS install

# Move file to final destination
mv -v $LFS/usr/bin/{lzma,unlzma,lzcat,xz,unxz,xzcat}  $LFS/bin
mv -v $LFS/usr/lib/liblzma.so.*                       $LFS/lib
ln -svf ../../lib/$(readlink $LFS/usr/lib/liblzma.so) $LFS/usr/lib/liblzma.so

# Remove unnecessary files
cd ..
rm -Rf xz-5.2.5
```

\newpage

### Installation de Binutils passe 2

```{.sh caption="Installation de Binutils passe 2"}
tar -xvf binutils-2.36.1.tar.xz
cd binutils-2.36.1

mkdir -v build
cd build

# Prepare Binutils pass 2 compilation
../configure                   \
    --prefix=/usr              \
    --build=$(../config.guess) \
    --host=$LFS_TGT            \
    --disable-nls              \
    --enable-shared            \
    --disable-werror           \
    --enable-64-bit-bfd

# Compile and install
make
make DESTDIR=$LFS install

# Apply fix
install -vm755 libctf/.libs/libctf.so.0.0.0 $LFS/usr/lib

# Remove unnecessary files
cd ../..
rm -Rf binutils-2.36.1
```

\newpage

### Installation de Gcc passe 2

```{.sh caption="Installation de Gcc passe 2"}
tar -xvf gcc-10.2.0.tar.xz
cd gcc-10.2.0

# Prepare necessary tools
tar -xf ../mpfr-4.1.0.tar.xz
mv -v mpfr-4.1.0 mpfr
tar -xf ../gmp-6.2.1.tar.xz
mv -v gmp-6.2.1 gmp
tar -xf ../mpc-1.2.1.tar.gz
mv -v mpc-1.2.1 mpc

# Apply change for x86_64 hosts
case $(uname -m) in
  x86_64)
    sed -e '/m64=/s/lib64/lib/' -i.orig gcc/config/i386/t-linux64
  ;;
esac

mkdir -v build
cd       build

# Create a symlink allowing libgcc to be built with posix threads support 
mkdir -pv $LFS_TGT/libgcc
ln -s ../../../libgcc/gthr-posix.h $LFS_TGT/libgcc/gthr-default.h

# Prepare Gcc pass 2 compilation
../configure                                       \
    --build=$(../config.guess)                     \
    --host=$LFS_TGT                                \
    --prefix=/usr                                  \
    CC_FOR_TARGET=$LFS_TGT-gcc                     \
    --with-build-sysroot=$LFS                      \
    --enable-initfini-array                        \
    --disable-nls                                  \
    --disable-multilib                             \
    --disable-decimal-float                        \
    --disable-libatomic                            \
    --disable-libgomp                              \
    --disable-libquadmath                          \
    --disable-libssp                               \
    --disable-libvtv                               \
    --disable-libstdcxx                            \
    --enable-languages=c,c++

# Compile and install
make
make DESTDIR=$LFS install

# Create utility simlink
ln -sv gcc $LFS/usr/bin/cc

# Remove unnecessary files
cd ../..
rm -Rf gcc-10.2.0
```

\newpage