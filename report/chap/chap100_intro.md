# Introduction

Ce travail pratique prend part au cours de **Conception OS** enseigné par Claudio Cortinovis.
A travers ce dernier TP, l'étudiant créé un **Linux From Scratch**.
Il s'agit là de construire sa propre distribution Linux à l'aide d'un tutoriel.

## Objectifs

Les objectifs du travail pratique sont les suivants [@pdfTP] :

- Créer sa distribution Linux
- Personnalisation la distribution créée

## Tutoriel

Ce travail pratique est effectué à l'aide de deux tutoriels :

1. Le célèbre tutoriel *Linux From Scratch* [@lfsTutoOfficial]
2. Tutoriel vidéo YouTube se basant sur LFS 10.1 [@lfsTutoYT]

\newpage
