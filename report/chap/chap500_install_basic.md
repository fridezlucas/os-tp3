# Construire le système LFS

Ce présent chapitre installe toutes les dépendances du système LFS sur la partition dédiée.

## Installations de logiciels de base

Comme de nombreux paquets doivent être installés, les commandes ne sont pas inscrites dans ce sous-chapitre. Cependant, elles se trouvent dans le chapitre 8 du *guide LFS*.

\newpage
