# Installation

Ce chapitre retranscrit l'installation de la machine virtuelle utilisée à travers ce travail pratique.

## Descriptif

La machine `fridez` est installée sur l'OS **EndeavourOS** comme présentée dans le tutoriel de Kernotex.

Ci-dessous se trouve la configuration de cette dernière :

: Configuration de la machine fridez

| Paramètre           | Valeur                                 |
|---------------------|----------------------------------------|
| Langue              | Anglais                                |
| Pays                | Suisse                                 |
| Timezone            | Region Europe / Zürich                 |
| Clavier             | Suisse (français)                      |
| Nom de machines     | `fridez`                               |
| Nom de domaine      | <vide>                                 |
| Root password       | `admin`                                |
| Username (non root) | Lucas Fridez                           |
| User (non root)     | `fridez`                               |
| Password (non root) | `fridez`                               |
| Disque principal    | 30 Go                                  |
| Partitions          | Partition Endeavour avec Swap          |
| Manager de paquets  | Suisse / deb.debian.org / pas de proxy |
| Environnement       | Standard System Utilities              |
| GRUB                | MBR sur /dev/sda1                      |
| Disque secondaire   | 30 Go                                  |
| CPU                 | 6 CPU                                  |
| RAM                 | 8 Go                                   |


\newpage
