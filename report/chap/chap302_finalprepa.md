## Préparation finale

Cette section effectue quelques tâches supplémentaires dans le but de préparer la construction du
système temporaire. Plusieurs répertoires sont créés dans le dossier `$LFS` pour les outils temporaires. [@final]

### Architecture de base

Désormais, il convient de créer une architecture de dossiers basiques sur la partition :

```{.sh caption="Création de l'architecture de base"}
mkdir -pv $LFS/{bin,etc,lib,sbin,usr,var,tools}
case $(uname -m) in
  x86_64) mkdir -pv $LFS/lib64 ;;
esac

mkdir -pv $LFS/tools
```

### Utilisateur du système LFS

Il convient de créer un utilisateur du système LFS :

```{.sh caption="Création de l'utilisateur du système LFS"}
groupadd lfs
useradd -s /bin/bash -g lfs -m -k /dev/null lfs

passwd lfs

# type password
lfs
lfs 

# Give lfs access to /mnt/lfs directories

chown -v lfs $LFS/{usr,lib,var,etc,bin,sbin,tools}
case $(uname -m) in
  x86_64) chown -v lfs $LFS/lib64 ;;
esac

chown -v lfs $LFS/sources
```

### Connection en tant que user lfs

```{.sh caption="Connexion en tant qu'utilisateur lfs"}
su - lfs
```

```{.sh caption="Préparation des fichiers de démarrage"}
cat > ~/.bash_profile << "EOF"
exec env -i HOME=$HOME TERM=$TERM PS1='\u:\w\$ ' /bin/bash
EOF

cat > ~/.bashrc << "EOF"
set +h
umask 022
LFS=/mnt/lfs
LC_ALL=POSIX
LFS_TGT=$(uname -m)-lfs-linux-gnu
PATH=/usr/bin
if [ ! -L /bin ]; then PATH=/bin:$PATH; fi
PATH=$LFS/tools/bin:$PATH
CONFIG_SITE=$LFS/usr/share/config.site
export LFS LC_ALL LFS_TGT PATH CONFIG_SITE
EOF
```

Puis, il faut contrôler que le fichier `bash.bashrc` est correctement initialisé :

```{.sh caption="Contrôle du fichier bash.bashrc"}
exit # to use root user
[ ! -e /etc/bash.bashrc ] || mv -v /etc/bash.bashrc /etc/bash.bashrc.NOUSE

su - lfs # login as lfs again
```

Enfin, pour compiler les utilitaires, il convient de charger le fichier `.bash_profile` : 

```{.sh caption="Chargement du fichier de profile"}
source ~/.bash_profile`
```

### Utilitaires de compilation

Afin de compiler de manière optimiser, il convient de mettre à disposition plusieurs CPUs :

```{.sh caption="Mise à disposition de plusieurs CPUs de manière automatique"}
nano ~/.bashrc
# add next line
# Allow building with 6 CPUs
export MAKEFLAGS='-j6'
```


\newpage