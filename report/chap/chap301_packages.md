## Packages et patches

Cette section inclut le téléchargement de packages pour construire un système Linux simple.
Les versions téléchargées sont des versions qui ne devraient pas poser de problèmes [@packages].

### Préparation du dossier de sources

Afin d'obtenir toutes les sources, il convient de les stocker dans un même dossier :

```{.sh caption="Création d'un dossier de sources"}
mkdir -v $LFS/sources
chmod -v a+wt $LFS/sources
```

### Récupération des paquets

```{.sh caption="Récupération des paquets"}
cd $LFS/sources

# Get all packages links
wget http://www.linuxfromscratch.org/lfs/view/10.1/wget-list

# Download all sources
wget --input-file=wget-list --continue --directory-prefix=$LFS/sources
```

Il convient de contrôler les checksums une fois tous les paquets téléchargés :

```{.sh caption="Contrôle des checksums"}

wget http://www.linuxfromscratch.org/lfs/view/10.1/md5sums

pushd $LFS/sources
  md5sum -c md5sums
popd
```

La sortie de la commande ci-dessus montre que toutes les signatures sont correctes.


\newpage
